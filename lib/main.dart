import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() => runApp(MyApp());
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter bateria android'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _batteryLevel = 'Click para ver la bateria';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          MaterialButton(
            onPressed: _getBatteryLevel,
            color: Colors.blueAccent,
            child: Padding(
              padding: EdgeInsets.all(16),
              child: Text('Click!!', style: TextStyle(color: Colors.white, fontSize: 35)),
            ),
          ),
          Container(
            height: 16,
          ),
          Text(this._batteryLevel, style: TextStyle(fontSize: 20))
        ],
      )
    );
  }
  static const batteryChannel = const MethodChannel('battery');

  Future<void> _getBatteryLevel() async{
    String batteryLevel;
    try {
      final int result = await batteryChannel.invokeMethod('getBatteryLevel');
      batteryLevel = 'Nivel de bateria $result % .';
    } on PlatformException catch (e) {
      batteryLevel = "Fallo en obtener el nivel de bateria: '${e.message}'.";
    }
    setState(() {
      _batteryLevel = batteryLevel;
    });
  }
}
